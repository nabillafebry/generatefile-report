package id.co.asyst.amala.generatefile.report.utils;

import com.google.api.client.util.Value;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.*;
import org.apache.camel.Exchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Utils {
    private static Logger log = LogManager.getLogger(Utils.class);


    public void getBody(Exchange exchange){
        Map<String, Object> bodyQueue = (Map<String, Object>) exchange.getProperty("bodyQueue");

        String type = (String) bodyQueue.get("type");
        String query = (String) bodyQueue.get("query");
        String userid = (String) bodyQueue.get("userid");
        String modul = (String) bodyQueue.get("modul");
        String count = (String) bodyQueue.get("count");
        String idgeneratereport = (String) bodyQueue.get("idgeneratereport");

        exchange.setProperty("type", type);
        exchange.setProperty("query", query);
        exchange.setProperty("userid", userid);
        exchange.setProperty("modul", modul);
        exchange.setProperty("count", Integer.valueOf(count));
        exchange.setProperty("idgeneratereport", idgeneratereport);
    }

    public static String generateId(String modul) {
        String id = modul;

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddhhmmss");
        LocalDateTime now = LocalDateTime.now();
        String datenow = dtf.format(now);

        Random rand = new Random();

        String[] arr = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        String randomkey = "";

        for (int i = 0; i < 5; ++i) {
            int random = rand.nextInt(10);
            randomkey = randomkey + arr[random];
        }

        id = id.concat(datenow);
        id = id.concat(randomkey);

        return id;
    }
}
