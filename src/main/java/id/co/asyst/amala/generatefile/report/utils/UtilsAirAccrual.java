package id.co.asyst.amala.generatefile.report.utils;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.*;
import com.opencsv.CSVWriter;
import org.apache.camel.Exchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import id.co.asyst.commons.core.utils.GeneratorUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class UtilsAirAccrual {
    private static Logger logger = LogManager.getLogger(UtilsAirAccrual.class);

    @Value("${file.location}")
    private String path;

    public void generateId(Exchange exchange){
        String modul = (String) exchange.getProperty("modul");

        String id = Utils.generateId(modul);

        exchange.setProperty("id", id);
    }

    public void countFile(Exchange exchange){
        List<Map<String, String>> airAccrualList = (List<Map<String, String>>) exchange.getProperty("listResult");
        Integer count = airAccrualList.size();
        int countfile = count / 50000;
        int moduluscount = count % 50000;
        List<Integer> listCount = new ArrayList<>();

        for (int i = 0; i< countfile; i++){
            listCount.add(i);
        }
        if (moduluscount != 0){
            listCount.add(listCount.size());
        }

        exchange.setProperty("listCount", listCount);
    }

    public void selectBigQuery(Exchange exchange){
        logger.info("SELECT BIGQUERY");
        String queryString = (String) exchange.getProperty("query");
        HashMap<String, String> accruals = new HashMap<>();
        List<Map<String, String>> listAiraccrual = new ArrayList<>();
        String query = queryString;
//        String gcp = path + "/amala-prd-4a58450b1f18.json";
        String gcp = "D://AMALA/report-app/gcp_keys/prod/amala-prd-4a58450b1f18.json";
        try {
            BigQuery bigquery = BigQueryOptions.newBuilder().setProjectId("amala-prd")
                    .setCredentials(
                            ServiceAccountCredentials.fromStream(new FileInputStream(gcp))
                    ).build().getService();

            QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(query).build();

            TableResult result = null;
            result = bigquery.query(queryConfig);
            for (FieldValueList row : bigquery.query(queryConfig).iterateAll()) {
                accruals = new HashMap<>();
                for (int i = 0; i < result.getSchema().getFields().size(); i++) {
                    if (row.get(i).getValue() != null) {
                        accruals.put(result.getSchema().getFields().get(i).getName(), row.get(i).getValue().toString());
                    } else {
                        accruals.put(result.getSchema().getFields().get(i).getName(), "-");
                    }
                }

                listAiraccrual.add(accruals);

            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        logger.info("FINISH SELECT BIGQUERY");
        exchange.setProperty("listResult", listAiraccrual);
    }

    public void generateFilename(Exchange exchange){
        String userid = (String) exchange.getProperty("userid");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDateTime now = LocalDateTime.now();
        String datenow = dtf.format(now);
        Integer indexfile = (Integer) exchange.getProperty("indexfile");
        Integer countfile = indexfile+1;

        String filename = "/airaccrual_" + datenow + "_" + userid + "_" + countfile + ".csv";

        exchange.setProperty("filename", filename);
    }

    public void generateFile(Exchange exchange){
        logger.info("START GENERATE FILE");
        List<Map<String, String>> airAccrualList = (List<Map<String, String>>) exchange.getProperty("listResult");
        String filename = (String) exchange.getProperty("filename");
        int indexfile = (int) exchange.getProperty("indexfile");

        int countStart = 0;
        int countEnd = 50000;

        for (int i=0; i<indexfile; i++){
            countStart = countStart+50000;
            countEnd = countEnd+50000;
        }

//        String filelocation = path + "/airaccrual";
        String filelocation = "D://test";

        int statusreport = 0;

        String[] header = {"transaction_id", "insert_date", "membership_number", "tier", "name", "surname", "email_address",
                "partner", "activity_classification", "dot", "org", "dest", "sector", "marketing_flight_number",
                "operating_flight_number", "booking_class", "tier_points", "award_points", "billing_base_points",
                "billing_bonus_points", "pax_name", "marketing_airline","operating_airline", "booking_code", "ticket_number",
                "flown_class", "frequency", "member_status", "inserted_by", "bo_code", "transaction_type", "ticketing_office"};

        SimpleDateFormat formatcontext = new SimpleDateFormat("dd-MM-yyyy");


        File file = new File(filelocation + filename);
        // create FileWriter object with file as parameter
        FileWriter outputfile = null;
        try {
            outputfile = new FileWriter(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // create CSVWriter with '|' as separator
        CSVWriter writer = new CSVWriter(outputfile, '|',
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);


        writer.writeNext(header);
        for (int i=countStart; i<countEnd; i++) {
            String[] stringAccrual = new String[32];
            boolean inBounds = (i >= 0) && (i < airAccrualList.size());
            if (inBounds){
                stringAccrual[0] = airAccrualList.get(i).get("transaction_id");
                stringAccrual[1] = airAccrualList.get(i).get("insert_date");
                stringAccrual[2] = airAccrualList.get(i).get("membership_number");
                stringAccrual[3] = airAccrualList.get(i).get("tier");
                stringAccrual[4] = airAccrualList.get(i).get("name");
                stringAccrual[5] = airAccrualList.get(i).get("surname");
                stringAccrual[6] = airAccrualList.get(i).get("email_address");
                stringAccrual[7] = airAccrualList.get(i).get("partner");
                stringAccrual[8] = airAccrualList.get(i).get("activity_classification");
                stringAccrual[9] = airAccrualList.get(i).get("dot");
                stringAccrual[10] = airAccrualList.get(i).get("org");
                stringAccrual[11] = airAccrualList.get(i).get("dest");
                stringAccrual[12] = airAccrualList.get(i).get("sector");
                stringAccrual[13] = airAccrualList.get(i).get("marketing_flight_number");
                stringAccrual[14] = airAccrualList.get(i).get("operating_flight_number");
                stringAccrual[15] = airAccrualList.get(i).get("booking_class");
                stringAccrual[16] = airAccrualList.get(i).get("tier_points");
                stringAccrual[17] = airAccrualList.get(i).get("award_points");
                stringAccrual[18] = airAccrualList.get(i).get("billing_base_points");
                stringAccrual[19] = airAccrualList.get(i).get("billing_bonus_points");
                stringAccrual[20] = airAccrualList.get(i).get("pax_name");
                stringAccrual[21] = airAccrualList.get(i).get("marketing_airline");
                stringAccrual[22] = airAccrualList.get(i).get("operating_airline");
                stringAccrual[23] = airAccrualList.get(i).get("booking_code");
                stringAccrual[24] = airAccrualList.get(i).get("ticket_number");
                stringAccrual[25] = airAccrualList.get(i).get("flown_class");
                stringAccrual[26] = airAccrualList.get(i).get("frequency");
                stringAccrual[27] = airAccrualList.get(i).get("member_status");
                stringAccrual[28] = airAccrualList.get(i).get("inserted_by");
                stringAccrual[29] = airAccrualList.get(i).get("bo_code");
                stringAccrual[30] = airAccrualList.get(i).get("transaction_type");
                stringAccrual[31] = airAccrualList.get(i).get("ticketing_office");


                writer.writeNext(stringAccrual);
            }
        }

        try {
            writer.close();
            statusreport = 1;
        } catch (IOException e) {
            e.printStackTrace();
            statusreport = 4;
        }

        exchange.setProperty("statusreport", statusreport);
        logger.info("FINISH GENERATE FILE");
    }
}
