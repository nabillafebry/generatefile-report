package id.co.asyst.amala.generatefile.report.utils;

public class HandlingValidateException extends Exception {

    public HandlingValidateException(String message) {
        super(message);
    }
}
